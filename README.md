Unity Volume Rendering
=====================

[Download compiled project here.](https://bitbucket.org/aratcliff/vres-volume-rendering-source-code/raw/d74055882b26a78e1031cafda4d7690ec30ca120/VolumeRendering.zip)

Volume rendering by object space raymarching for Unity.  

Slice through volume with x, y, z, sliders.  

Create new volumes from a folder of image slices or from a .mat file which contains a struct with an x_coords, y_coords, z_coords and data variable.  

## Compatibility

Tested on Unity 2018.2.18f1, Windows 10.

## Sources

- Original raymarching code provided by [GitHub user mattatz](https://github.com/mattatz/unity-volume-rendering)
- primitive: blog | object space raymarching - http://i-saint.hatenablog.com/entry/2015/08/24/225254
- The Volume Library - http://lgdv.cs.fau.de/External/vollib/
- Graphics Runner : Volume Rendering 101 - http://graphicsrunner.blogspot.jp/2009/01/volume-rendering-101.html
