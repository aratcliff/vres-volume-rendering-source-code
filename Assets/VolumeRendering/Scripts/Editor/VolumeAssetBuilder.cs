﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using UnityEngine;
using UnityEditor;
using Accord.IO;
using Accord.Math;
using System;

namespace VolumeRendering
{

    public class VolumeAssetBuilder : EditorWindow
    {

        [MenuItem("Window/VolumeAssetBuilder")]
        static void Init()
        {
            var window = EditorWindow.GetWindow(typeof(VolumeAssetBuilder));
            window.Show();
        }

        string inputPath, outputPath;
        int blendLevel = 2;
        UnityEngine.Object asset;

        void OnEnable()
        {
            inputPath = "";
            outputPath = "Assets/A.asset";
        }

        void OnGUI()
        {
            const float headerSize = 120f;

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Input folder path", GUILayout.Width(headerSize));
                asset = EditorGUILayout.ObjectField(asset, typeof(UnityEngine.Object), true);
                inputPath = AssetDatabase.GetAssetPath(asset);
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Blend level", GUILayout.Width(headerSize));
                blendLevel = EditorGUILayout.IntField(blendLevel);
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Output path", GUILayout.Width(headerSize));
                outputPath = EditorGUILayout.TextField(outputPath);
            }

            if (GUILayout.Button("Build volume"))
            {
                Build(inputPath, outputPath, blendLevel);
            }

            if (GUILayout.Button("Build MOE"))
            {
                BuildMOE(inputPath, outputPath);
            }

            if (GUILayout.Button("Build plank moe"))
            {
                BuildPlankMOE(inputPath, outputPath);
            }
        }

        void Build(string inputPath, string outputPath, int blendLevel)
        {
            var volume = Build(inputPath, blendLevel);
            AssetDatabase.CreateAsset(volume, outputPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log(string.Format("Asset created at {0}", outputPath));
        }

        void BuildMOE(string inputPath, string outputPath)
        {
            var volume = BuildMOE(inputPath);
            AssetDatabase.CreateAsset(volume, outputPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log(string.Format("MOE Asset created at {0}", outputPath));
        }

        public static Texture3D Build(string path, int blendLevel)
        {
            string folder = "Assets/VolumeRendering/Volumes/Billet_2/";

            //Get all images
            DirectoryInfo di = new DirectoryInfo(folder);
            FileInfo[] files = di.GetFiles("*.tif");

            int firstFile, filesLength;

            if (files.Length > 2048)
            {
                filesLength = 2048;
                firstFile = ((files.Length - filesLength) / 2);
            }
            else
            {
                filesLength = files.Length;
                firstFile = 0;
            }

            //Create texture
            Bitmap image = new Bitmap(folder + files[firstFile].Name);
            Texture3D texture = new Texture3D(
                image.Width / blendLevel,
                image.Height / blendLevel,
                filesLength,
                TextureFormat.RGBA32,
                false
            );

            int imgLength = (image.Width / blendLevel) * (image.Height / blendLevel) * filesLength;
            UnityEngine.Color[] colors = new UnityEngine.Color[imgLength];

            Debug.Log(string.Format("There are {0} files being loaded with {1}x blending",
                filesLength,
                blendLevel
            ));

            int counter = 0;
            const float inv = 1.0f / 255.0f;
            UnityEngine.Color clear = new UnityEngine.Color(0f, 0f, 0f, 0f);
            //Loop through images, each image becomes 1pixel on z-dimension
            for (int z = firstFile; z < filesLength + firstFile; z++)
            {
                if (z > firstFile)
                {
                    image = new Bitmap(folder + files[z].Name);
                }

                //Loop through each images pixels on the x- and y- axis averaging each colour in an squared area
                for (int x = 0; x < image.Width / blendLevel; x++)
                {
                    for (int y = 0; y < image.Height / blendLevel; y++)
                    {
                        //int r = 0, g = 0, b = 0, a = 0;
                        //for (int i = 0; i < blendLevel; i++)
                        //{
                        //    for (int j = 0; j < blendLevel; j++)
                        //    {
                        //        r += image.GetPixel(x * blendLevel + i, y * blendLevel + j).R;
                        //        g += image.GetPixel(x * blendLevel + i, y * blendLevel + j).G;
                        //        b += image.GetPixel(x * blendLevel + i, y * blendLevel + j).B;
                        //        a += image.GetPixel(x * blendLevel + i, y * blendLevel + j).A;
                        //    }
                        //}
                        //colors[counter] = new UnityEngine.Color(
                        //    (r/(blendLevel*blendLevel)) * inv, 
                        //    (g/(blendLevel*blendLevel)) * inv, 
                        //    (b/(blendLevel*blendLevel)) * inv, 
                        //    (a/(blendLevel*blendLevel)) * inv
                        //);
                        System.Drawing.Color color = image.GetPixel(x * blendLevel, y * blendLevel);

                        if ((color.R < 30 && color.G < 30 && color.B < 30) ||
                            (color.R > 240 && color.G > 240 && color.B > 240))
                        {
                            colors[counter] = clear;
                        }
                        else
                        {
                            colors[counter] = new UnityEngine.Color(
                                color.R * inv,
                                color.G * inv,
                                color.B * inv,
                                0.4f
                            );
                        }
                        counter++;
                    }
                }
            }

            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Bilinear;
            texture.anisoLevel = 0;
            texture.SetPixels(colors);
            texture.Apply();
            return texture;
        }
        
        public static Texture3D BuildMOE(string path)
        {
            string billetName = "tree_studs_combined";
            var reader = new MatReader("D:\\VRES\\Data\\"+billetName+".mat");
            var billet = reader[billetName];

            var colormapReader = new MatReader("D:\\VRES\\Data\\colormap.mat");
            var map = colormapReader["Colormap"];
            double[,] parula = map["parula"].GetValue<double[,]>();

            double[,] x_coords = billet["x_coords"].GetValue<double[,]>();
            double[,] y_coords = billet["y_coords"].GetValue<double[,]>();
            double[,] z_coords = billet["z_coords"].GetValue<double[,]>();
            double[,] moe = billet["moe"].GetValue<double[,]>();
            var xcoords = x_coords.Cast<Double>();
            var ycoords = y_coords.Cast<Double>();
            var zcoords = z_coords.Cast<Double>();
            var moe2 = moe.Cast<Double>();

            double xmin = xcoords.Min();
            double ymin = ycoords.Min();

            double xmax = xcoords.Max();
            double ymax = ycoords.Max();
            double zmax = zcoords.Max();

            double scalefac = 0.1;

            int roundedXMax = (int)(Math.Round((xmax - xmin) * scalefac));
            int roundedYMax = (int)(Math.Round((ymax - ymin) * scalefac));
            int roundedZMax = (int)(Math.Round(zmax * scalefac));

            Texture3D texture = new Texture3D(
                roundedXMax, roundedYMax, roundedZMax,
                TextureFormat.RGBA32,
                false
            );

            UnityEngine.Color[] colors = new UnityEngine.Color[
                roundedXMax * roundedYMax * roundedZMax
            ];

            UnityEngine.Color clear = new UnityEngine.Color(0f, 0f, 0f, 0f);

            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = clear;
            }

            double minMoe = moe2.Min();
            double difMoe = moe2.Max() - minMoe;

            for (int i = 0; i < x_coords.GetLength(0); i++)
            {
                for (int j = 0; j < x_coords.GetLength(1); j++)
                {
                    int x = (int)Math.Round((x_coords[i, j] - xmin) * scalefac);
                    int y = (int)Math.Round((y_coords[i, j] - ymin) * scalefac);
                    int z = (int)Math.Round(z_coords[i, j] * scalefac);
                    double moeVal = moe[i, j];

                    //Scale colour
                    double moePercent = (moeVal - minMoe) / difMoe;
                    int moeColor = (int)Math.Floor(moePercent * 7499000);
                    
                    //Add colour to array
                    if (roundedXMax * roundedYMax * roundedZMax >= x + (y * roundedXMax) + (z * roundedXMax * roundedYMax))
                    {
                        colors[
                            x +
                            (y * roundedXMax) +
                            (z * roundedXMax * roundedYMax)
                        ] = new UnityEngine.Color(
                            //r * inv, g * inv, b * inv, 1f
                            (float)parula[moeColor, 0], (float)parula[moeColor, 1], (float)parula[moeColor, 2], 1f
                        );
                    }
                }
            }           

            //Fill in z-axis
            for (int i = roundedXMax * roundedZMax; i < colors.Length; i++)
            {
                UnityEngine.Color prevColor = colors[i - (roundedXMax * roundedYMax)];
                if (prevColor != clear)
                {
                    colors[i] = prevColor;
                }
            }

            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Bilinear;
            texture.anisoLevel = 0;
            texture.SetPixels(colors);
            texture.Apply();
            return texture;
        }

        public static void BuildPlankMOE(string path, string outputPath)
        {
            var colormapReader = new MatReader("D:\\VRES\\Data\\colormap.mat");
            var map = colormapReader["Colormap"];
            double[,] parula = map["parula"].GetValue<double[,]>();

            string folder = "D:\\VRES\\Data\\tree\\";
            DirectoryInfo di = new DirectoryInfo(folder);
            FileInfo[] files = di.GetFiles("*.mat");

            foreach (var file in files)
            {
                var reader = new MatReader(folder + file.Name);
                var stud = reader["stud"];

                Debug.Log(file.Name.Substring(0, file.Name.Length - 4));

                double[,] x_coords = stud["x_coords"].GetValue<double[,]>();
                double[,] y_coords = stud["y_coords"].GetValue<double[,]>();
                UInt16[,] z_coords = stud["z_coords"].GetValue<UInt16[,]>();
                double[,] moe = stud["moe"].GetValue<double[,]>();

                Debug.Log(stud["z_coords"].ValueType);


                var xcoords = x_coords.Cast<Double>();
                var ycoords = y_coords.Cast<Double>();
                var zcoords = z_coords.Cast<UInt16>();
                var moe2 = moe.Cast<Double>();

                double xmin = xcoords.Min();
                double ymin = ycoords.Min();
                double zmin = zcoords.Min();

                double xmax = xcoords.Max();
                double ymax = ycoords.Max();
                double zmax = zcoords.Max();

                int roundedXMax = (int)(Math.Round(xmax - xmin));
                int roundedYMax = (int)(Math.Round(ymax - ymin));
                int roundedZMax = (int)(Math.Round(zmax - zmin));

                Texture3D texture = new Texture3D(
                    roundedXMax, roundedYMax, roundedZMax,
                    TextureFormat.RGBA32,
                    false
                );

                UnityEngine.Color[] colors = new UnityEngine.Color[
                    roundedXMax * roundedYMax * roundedZMax
                ];

                UnityEngine.Color clear = new UnityEngine.Color(0f, 0f, 0f, 0f);

                for (int i = 0; i < colors.Length; i++)
                {
                    colors[i] = clear;
                }

                double minMoe = moe2.Min();
                double difMoe = moe2.Max() - minMoe;
                int counter = 0;
                for (int i = 0; i < x_coords.GetLength(0); i++)
                {
                    for (int j = 0; j < x_coords.GetLength(1); j++)
                    {
                        int x = (int)Math.Floor((x_coords[i, j] - xmin));
                        int y = (int)Math.Floor((y_coords[i, j] - ymin));
                        int z = (int)Math.Floor(z_coords[i, j] - zmin);
                        double moeVal = moe[i, j];

                        //Scale colour
                        double moePercent = (moeVal - minMoe) / difMoe;
                        int moeColor = (int)Math.Floor(moePercent * 7499000);

                        //Add colour to array
                        if (roundedXMax * roundedYMax * roundedZMax >= x + (y * (roundedXMax) + (z * (roundedXMax) * (roundedYMax))))
                        {
                            colors[
                                x +
                                (y * (roundedXMax)) +
                                (z * (roundedXMax) * (roundedYMax))
                            ] = new UnityEngine.Color(
                                (float)parula[moeColor, 0], (float)parula[moeColor, 1], (float)parula[moeColor, 2], 1f
                            );
                        }
                        else
                        {
                            counter++;
                        }
                    }
                }

                //Fill in z-axis
                for (int i = roundedXMax * roundedZMax; i < colors.Length; i++)
                {
                    UnityEngine.Color prevColor = colors[i - (roundedXMax * roundedYMax)];
                    if (prevColor.a != 0)
                    {
                        colors[i] = prevColor;
                    }
                }

                texture.wrapMode = TextureWrapMode.Clamp;
                texture.filterMode = FilterMode.Bilinear;
                texture.anisoLevel = 0;
                texture.SetPixels(colors);
                texture.Apply();

                AssetDatabase.CreateAsset(texture, "Assets/VolumeRendering/Textures/TreeStuds/" + file.Name.Substring(0, file.Name.Length - 4) + ".asset");
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log("Moe plank assets created at Assets/VolumeRendering/Textures/TreeStuds");
        }
    }
}


