﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Accord.IO;

namespace VolumeRendering
{

    public class VolumeRenderingController : MonoBehaviour {

        [SerializeField] protected VolumeRendering volume;
        [SerializeField] protected Slider sliderXMin, sliderXMax, sliderYMin, sliderYMax, sliderZMin, sliderZMax;
        [SerializeField] protected Button genColoursButton, genSlicedButton, genMatlabButton;
        [SerializeField] protected Transform axis;
        [SerializeField] protected Dropdown renderPicker;
        [SerializeField] protected InputField directory, variable, scale;
        [SerializeField] protected int pickedRender;

        void Start ()
        {
            const float threshold = 0.025f;

            sliderXMin.onValueChanged.AddListener((v) => {
                volume.sliceXMin = sliderXMin.value = Mathf.Min(v, volume.sliceXMax - threshold);
            }); 
            sliderXMax.onValueChanged.AddListener((v) => {
                volume.sliceXMax = sliderXMax.value = Mathf.Max(v, volume.sliceXMin + threshold);
            });
            
            sliderYMin.onValueChanged.AddListener((v) => {
                volume.sliceYMin = sliderYMin.value = Mathf.Min(v, volume.sliceYMax - threshold);
            });
            sliderYMax.onValueChanged.AddListener((v) => {
                volume.sliceYMax = sliderYMax.value = Mathf.Max(v, volume.sliceYMin + threshold);
            });

            sliderZMin.onValueChanged.AddListener((v) => {
                volume.sliceZMin = sliderZMin.value = Mathf.Min(v, volume.sliceZMax - threshold);
            });
            sliderZMax.onValueChanged.AddListener((v) => {
                volume.sliceZMax = sliderZMax.value = Mathf.Max(v, volume.sliceZMin + threshold);
            });
             
            genColoursButton.onClick.AddListener(() => GenColours());
            genSlicedButton.onClick.AddListener(() => GenSlices());
            genMatlabButton.onClick.AddListener(() => GenMatlab());
        }

        void Update()
        {
            volume.axis = axis.rotation;
        }

        public void FinishTextureGen(Texture3D tex, UnityEngine.Color[] colors)
        {
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.filterMode = FilterMode.Bilinear;
            tex.anisoLevel = 0;
            tex.SetPixels(colors);
            tex.Apply();

            volume.material.SetTexture("_Volume", tex);
        }

        public void GenColours()
        {
            if (renderPicker.value == pickedRender)
            {
                int lim = 256;
                Texture3D tex = new Texture3D(lim, lim, lim, TextureFormat.RGBA32, false);
                UnityEngine.Color[] colors = new UnityEngine.Color[lim * lim * lim];
                int counter = 0;

                for (int i = 0; i < lim; i++)
                {
                    for (int j = 0; j < lim; j++)
                    {
                        for (int k = 0; k < lim; k++)
                        {
                            if (Mathf.Pow(i - (lim / 2), 2) + Mathf.Pow(j - (lim / 2), 2) + Mathf.Pow(k - (lim / 2), 2) <= Mathf.Pow(lim / 2, 2))
                            {
                                colors[counter] = new UnityEngine.Color(i / 255f, j / 255f, k / 255f, 1f);
                            }
                            else
                            {
                                colors[counter] = new UnityEngine.Color(0f, 0f, 0f, 0f);
                            }
                            counter++;
                        }
                    }
                }

                FinishTextureGen(tex, colors);
                
                volume.transform.localScale = new Vector3(lim / 100f, lim / 100f, lim / 100f);
            }
        }

        public void GenSlices()
        {
            if (renderPicker.value == pickedRender)
            {
                //int blendLevel = int.Parse(scale.text);
                int blendLevel = 2;

                string folder = directory.text;
                if (!folder.EndsWith("\\")) folder += "\\";
                DirectoryInfo di = new DirectoryInfo(folder);
                FileInfo[] files = di.GetFiles("*.tif");

                int firstFile, filesLength;

                if (files.Length > 2048)
                {
                    filesLength = 2048;
                    firstFile = ((files.Length - filesLength) / 2);
                }
                else
                {
                    filesLength = files.Length;
                    firstFile = 0;
                }

                Bitmap image = new Bitmap(folder + files[firstFile].Name);

                Texture3D tex = new Texture3D(
                    image.Width / blendLevel,
                    image.Height / blendLevel,
                    filesLength,
                    TextureFormat.RGBA32,
                    false
                );

                int imgLength = (image.Width / blendLevel) * (image.Height / blendLevel) * filesLength;
                UnityEngine.Color[] colors = new UnityEngine.Color[imgLength];

                int counter = 0;
                const float inv = 1f / 255f;

                for (int z = firstFile; z < filesLength + firstFile; z++)
                {
                    if (z > firstFile) { image = new Bitmap(folder + files[z].Name); }
                    for (int x = 0; x < image.Width / blendLevel; x++)
                    {
                        for (int y = 0; y < image.Height / blendLevel; y++)
                        {
                            System.Drawing.Color color = image.GetPixel(x * blendLevel, y * blendLevel);

                            if ((color.R < 30 && color.G < 30 && color.B < 30) ||
                                (color.R > 240 && color.G > 240 && color.B > 240))
                            {
                                colors[counter] = new UnityEngine.Color(0, 0, 0, 0);
                            }
                            else
                            {
                                colors[counter] = new UnityEngine.Color(
                                    color.R * inv,
                                    color.G * inv,
                                    color.B * inv,
                                    1f
                                );
                            }
                            counter++;
                        }
                    }
                }

                FinishTextureGen(tex, colors);

                volume.transform.localScale = new Vector3((image.Width / blendLevel) / 100f, (image.Height / blendLevel) / 100f, filesLength / 200f);
            }
        }

        public void GenMatlab()
        {
            if (renderPicker.value == pickedRender)
            {
                var colormapReader = new MatReader("Assets/Resources/colormap.mat");
                var colormap = colormapReader["Colormap"];
                //
                string colormapType = "parula";
                //
                double[,] map = colormap[colormapType].GetValue<double[,]>();


                string file = directory.text;
                var reader = new MatReader(file);
                string variableName = variable.text;
                var containingStruct = reader[variableName];

                //
                string x_coords_name = "x_coords";
                string y_coords_name = "y_coords";
                string z_coords_name = "z_coords";
                string data_name = "moe";
                //

                double[,] x_coords = containingStruct[x_coords_name].GetValue<double[,]>();
                double[,] y_coords = containingStruct[y_coords_name].GetValue<double[,]>();
                double[,] z_coords = containingStruct[z_coords_name].GetValue<double[,]>();
                double[,] data = containingStruct[data_name].GetValue<double[,]>();

                var xcoords = x_coords.Cast<Double>();
                var ycoords = y_coords.Cast<Double>();
                var zcoords = z_coords.Cast<Double>();
                var datasingle = data.Cast<Double>();

                double xmin = xcoords.Min();
                double ymin = ycoords.Min();
                double xmax = xcoords.Max();
                double ymax = ycoords.Max();
                double zmax = zcoords.Max();

                double scaleFactor = double.Parse(scale.text);

                int roundedXMax = (int)(Math.Round((xmax - xmin) * scaleFactor));
                int roundedYMax = (int)(Math.Round((ymax - ymin) * scaleFactor));
                int roundedZMax = (int)(Math.Round((zmax * scaleFactor)));

                Texture3D tex = new Texture3D(
                    roundedXMax, roundedYMax, roundedZMax,
                    TextureFormat.RGBA32, false
                );

                UnityEngine.Color[] colors = new UnityEngine.Color[roundedXMax * roundedYMax * roundedZMax];
                UnityEngine.Color clear = new UnityEngine.Color(0f, 0f, 0f, 0f);

                for (int i = 0; i < colors.Length; i++) colors[i] = clear;

                double minData = datasingle.Min();
                double difData = datasingle.Max() - minData;

                for (int i = 0; i < x_coords.GetLength(0); i++)
                {
                    for (int j = 0; j < x_coords.GetLength(1); j++)
                    {
                        int x = (int)Math.Round((x_coords[i, j] - xmin) * scaleFactor);
                        int y = (int)Math.Round((y_coords[i, j] - ymin) * scaleFactor);
                        int z = (int)Math.Round(z_coords[i, j] * scaleFactor);
                        double dataVal = data[i, j];

                        //Scale colour
                        double dataPercent = (dataVal - minData) / difData;
                        int dataColor = (int)Math.Floor(dataPercent * (100000 - 1));

                        //Add colour to array
                        for (int m = z - 1; m < z + 2; m++)
                        {
                            for (int n = y - 1; n < y + 2; n++)
                            {
                                for (int o = x - 1; o < x + 2; o++)
                                {
                                    int arrayPos = o + (n * roundedXMax) + (m * roundedXMax * roundedYMax);
                                    if (arrayPos >= 0 && arrayPos < roundedXMax * roundedYMax * roundedZMax)
                                    {
                                        colors[arrayPos] = new UnityEngine.Color(
                                            (float)map[dataColor, 0], (float)map[dataColor, 1], (float)map[dataColor, 2], 1f
                                        );
                                    }
                                }
                            }
                        }
                    }
                }

                FinishTextureGen(tex, colors);
                volume.transform.localScale = new Vector3(roundedXMax / 100f, roundedYMax / 100f, roundedZMax / 100f);
            }
        }
    }

}
